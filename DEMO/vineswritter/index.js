import opentype from 'opentype.js'

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// VINES ANIMATION FUNCTIONS /////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// <editor-fold>

// Instead of pruning branches randomly, we prune the ones furthest from the lattice.
// After we calculate each new control point, we need to determine how far it is from the lattice.
// The following function returns the distance from a point to a line segment --> http://geomalgorithms.com/a02-_lines.html
function distancePointToLine(point, line) {

    // Length of line segment
    let L = Math.sqrt(Math.pow(line[1].x - line[0].x, 2) + Math.pow(line[1].y - line[0].y, 2));

    // Calculate position of projection along line segment
    let r = ((point.x - line[0].x) * (line[1].x - line[0].x) + (point.y - line[0].y) * (line[1].y - line[0].y)) / Math.pow(L, 2);

    // Calculate distance of point to projection
    let s = ((line[0].y - point.y) * (line[1].x - line[0].x) - (line[0].x - point.x) * (line[1].y - line[0].y)) / Math.pow(L, 2);

    // Calculate perpendicular projection of point on line
    if (r >= 0 && r <= 1) {
        return Math.abs(s) * L;
    } else {
        return Math.min(
            Math.sqrt(Math.pow(point.x - line[0].x, 2) + Math.pow(point.y - line[0].y, 2)),
            Math.sqrt(Math.pow(point.x - line[1].x, 2) + Math.pow(point.y - line[1].y, 2))
        );
    }
}

// Given a lattice of points it will start growing vines around the segments of the lattice
function drawVineWithLattice(context, lattice, x, y, interactions, sort, prune, minLength, maxLength) {

    // Create initial branch
    let branches = [];
    branches.push({
        points: [{x: x, y: y}, {x: x, y: y}, {x: x, y: y}, {x: x, y: y}],
        angle: 0,
        distanceToLattice: 1000
    });

    // Start at t = 0
    let t = 0;

    // Animation function
    function animate() {

        // Draw branches
        for (let i in branches) {

            // Draw spline segment
            let ax = (-branches[i].points[0].x + 3 * branches[i].points[1].x - 3 * branches[i].points[2].x + branches[i].points[3].x) / 6;
            let ay = (-branches[i].points[0].y + 3 * branches[i].points[1].y - 3 * branches[i].points[2].y + branches[i].points[3].y) / 6;
            let bx = ( branches[i].points[0].x - 2 * branches[i].points[1].x + branches[i].points[2].x) / 2;
            let by = ( branches[i].points[0].y - 2 * branches[i].points[1].y + branches[i].points[2].y) / 2;
            let cx = (-branches[i].points[0].x + branches[i].points[2].x) / 2;
            let cy = (-branches[i].points[0].y + branches[i].points[2].y) / 2;
            let dx = ( branches[i].points[0].x + 4 * branches[i].points[1].x + branches[i].points[2].x) / 6;
            let dy = ( branches[i].points[0].y + 4 * branches[i].points[1].y + branches[i].points[2].y) / 6;
            context.beginPath();
            context.moveTo(
                ax * Math.pow(t, 3) + bx * Math.pow(t, 2) + cx * t + dx,
                ay * Math.pow(t, 3) + by * Math.pow(t, 2) + cy * t + dy
            );
            context.lineTo(
                ax * Math.pow(t + 0.1, 3) + bx * Math.pow(t + 0.1, 2) + cx * (t + 0.1) + dx,
                ay * Math.pow(t + 0.1, 3) + by * Math.pow(t + 0.1, 2) + cy * (t + 0.1) + dy
            );
            context.stroke();
            context.closePath();
        }

        // Advance t
        t += 0.1;

        // When finished drawing splines, create a new set of branches
        if (t >= 1) {

            // Create array to store next iteration of branchces
            let new_branches = [];

            // Iterate over each branch
            for (let j in branches) {

                // Replace with 2 new branches
                for (let k = 0; k < 2; k++) {

                    // Generate random deviation from previous angle
                    let angle = branches[j].angle - (Math.random() * 180 - 90);

                    // Determine closest lattice point
                    let distance = 100000;
                    for (let l in lattice) {
                        let result = distancePointToLine(branches[j].points[3], lattice[l]);
                        if (result < distance) distance = result;
                    }

                    // Generate random length
                    let length = Math.random() * (maxLength - minLength) + minLength;

                    // Calculate new point
                    let x2 = branches[j].points[3].x + Math.sin(Math.PI * angle / 180) * length;
                    let y2 = branches[j].points[3].y - Math.cos(Math.PI * angle / 180) * length;

                    // Add to new branch array
                    new_branches.push({
                        points: [
                            branches[j].points[1],
                            branches[j].points[2],
                            branches[j].points[3],
                            {x: x2, y: y2}
                        ],
                        angle: angle,
                        distanceToLattice: distance
                    });
                }
            }

            // Sort branches by distance to lattice
            new_branches.sort(function (a, b) {
                return a.distanceToLattice - b.distanceToLattice;
            });

            // If over 10 branches, prune the branches furthest from the lattice
            if (prune) {
                if (sort) {
                    while (new_branches.length > 20) new_branches.pop();
                } else {
                    while (new_branches.length > 20) {
                        new_branches.splice(Math.floor(Math.random() * new_branches.length), 1);
                    }
                }
            }

            // Replace old branch array with new
            branches = new_branches;

            // Restart drawing splines at t=0
            interactions--;
            t = 0;
        }

        // Keep on animating
        if (interactions > 0) {
            handles[handle] = requestAnimationFrame(animate);
        }
    } // End of animation function.


    // Start animation and return handle
    let handle = requestAnimationFrame(animate);
    handles[handle] = handle;
    return handle;
}

// This method returns the last element of an array (not used).
let last = function (array, n) {
    if (array === null) return void 0;
    if (n === null) return array[array.length - 1];
    return array.slice(Math.max(array.length - n, 0));
};

// This method returns the middle element of any array.
let middle = function (array) {
    if (array === null) return void 0;

    let length = array.length;
    let middle_key = Math.floor(length / 2);
    return array[middle_key];
};

function nearestEvenNumber(number) {
    return (2 * Math.round(number / 2));
}

/**
 * Here is the sample code that calculates width, actualBoundingBoxAscent,
 * actualBoundingBoxDescent, fontBoundingBoxAscent and fontBoundingBoxDescent.
 * https://developer.tizen.org/community/tip-tech/working-fonts-using-opentype.js?langswitch=en
 */
let measureText = function (font, fontSize, text) {

    let ascent = 0;
    let descent = 0;
    let width = 0;
    // Firstly, we have to calculate a scale by which we will multiple all paths' points,
    // to get the actual point in space for the given font size.
    let scale = 1 / font.unitsPerEm * fontSize;
    //Later, we have to get paths for each letter/glyph using the font.stringToGlyphs() function.
    let glyphs = font.stringToGlyphs(text);

    for (let i = 0; i < glyphs.length; i++) {
        let glyph = glyphs[i];
        if (glyph.advanceWidth) {
            width += glyph.advanceWidth * scale;
        }
        if (i < glyphs.length - 1) {
            let kerningValue = font.getKerningValue(glyph, glyphs[i + 1]);
            width += kerningValue * scale;
        }
        ascent = Math.max(ascent, glyph.yMax);
        descent = Math.min(descent, glyph.yMin);
    }

    return {
        width: width,
        actualBoundingBoxAscent: ascent * scale,
        actualBoundingBoxDescent: descent * scale,
        fontBoundingBoxAscent: font.ascender * scale,
        fontBoundingBoxDescent: font.descender * scale
    };
};

// This array store the handler of any animation triggered, so we can manage them in case we need to.
let handles = [];

export function showVinesAnimation(text, fontsize = 160, fontURL) {

    // Padding for the canvas.
    let padding = 30;
    // Vines line Width.
    let lineWidth = 0.1;

    // Now lets grow some vines:

    // This is only conpatible with Chrome and Firefox.
    // Charging the font to predict the width of the text we are going to animate.
    //let f = new FontFace("Roboto-Thin", "url(<?php echo $rootpath; ?>fonts/Roboto-Thin.ttf)");
    //  f.load().then(function() {
    //    // Ready to use the font in a canvas context
    // });


    // Here we use the opentype.js library to convert the glyphs of the font
    // into paths taken the curves that define the each glyph and converting
    // them into lattices (array of segments given for a pair of points each),
    // to draw then the vines around that "invisible" lattice.
    opentype.load(fontURL, function (err, font) {
        if (err) {
            console.log('Could not load font: ' + err);
            console.log(fontURL);
        } else {


            // Add logic to change fotsize and make the canvas width 72% of screen width.
            // 958px -> 1366px
            // $(window).width()
            // We now fontsize is around 160px for a screen of 1366p width, so if width is bigger
            // than that we will increase fontsize till it match aprox 72% of screen width.


            // Dinamically create canvas.
            let canvas = document.getElementById("myCanvas")
            let parentNode = canvas.parentNode;
            if (canvas) {
                parentNode.removeChild(canvas);
            }
            canvas = document.createElement('canvas');
            canvas.id = "myCanvas";
            canvas.style.zIndex = 8;


            // Getting the context:
            let ctx = canvas.getContext('2d');

            // Getting the aprox lenhgt of the canvas content:
            ctx.font = fontsize + "px";

            // Canvas width and height according to the text. As the canvas is meant to be
            // centered on screen, we need both values to be an even number, son we can
            // translate the canvas -50% on X axis.
            let textBoundings = measureText(font, fontsize, text);
            canvas.width = nearestEvenNumber(textBoundings.width + padding);
            canvas.height = nearestEvenNumber(fontsize + padding);
            canvas.style.width = canvas.width + "px";
            canvas.style.height = canvas.height + "px";

            //  Append it to DOM inside original container.
            parentNode.appendChild(canvas);

            // Start calculating the latttices to grow the vines.
            // This variable will store the paths array, one for each glyph.
            let paths;
            // This one will store the lattice related with each glyph.
            let lattice = [];


            // Use your font here --> getPaths (text, x, y, fontSize, options)
            // Returns an array of paths. One for each glyph.
            paths = font.getPaths(text, 12, textBoundings.fontBoundingBoxAscent, fontsize);


            // Now we create the segments form the points defined in paths:
            let glyph, point;
            for (let i = 0; i < paths.length; i++) {
                glyph = paths[i];

                for (let j = 0; j < glyph.commands.length; j++) {

                    let point = glyph.commands[j];
                    let length = lattice.length;
                    let next_point = glyph.commands[j + 1];

                    // point.type == M marks the begining and end of a segment --> ctx.moveTo()
                    // point.type == Z marks the end of the glyph.             --> ctx.closePath()
                    // point.type == L marks a linear line                     --> ctx.lineTo()
                    // point.type == Q marks a quadratic line                  --> ctx.quadraticCurveTo()
                    // point.type == C marks a cubic line                      --> ctx.bezierCurveTo()
                    if (point.type == 'M') {

                        lattice[length] = [];
                        lattice[length].push([{x: point.x, y: point.y}, {x: next_point.x, y: next_point.y}]);

                        // End point of the current glyph:
                    } else if (point.type == 'L' || point.type == 'Q' || point.type == 'Q') {
                        if (next_point.type != 'M' && next_point.type != 'Z')
                            lattice[length - 1].push([{x: point.x, y: point.y}, {x: next_point.x, y: next_point.y}]);

                    }
                }
            }

            // Grow the vines!!
            ctx.strokeStyle = 'rgb(24, 84, 24)';    // Vines line color. Logo color is rgb(127,194,105)
            ctx.lineWidth = lineWidth;              // Vines line width
            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) ctx.lineWidth = 0.25;
            let minLength = fontsize / 42;          // Min length of the vines.
            let maxLength = fontsize / 18;          // Max length of the vines.
            let interactions = 55;                  // Number of interactions.
            let animation = [];                     // Matrix to store all the animations created.
            for (let i in lattice) {

                animation.push(drawVineWithLattice(ctx, lattice[i], lattice[i][0][0].x, lattice[i][0][0].y, interactions, true, true, minLength, maxLength));
                animation.push(drawVineWithLattice(ctx, lattice[i], middle(lattice[i])[0].x, middle(lattice[i])[0].y, interactions, true, true, minLength, maxLength));
            }
        }
    });
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// /VINES ANIMATION FUNCTIONS //////////////////////////////////////////////////////////////</editor-fold>
//////////////////////////////////////////////////////////////////////////////////////////////////////////